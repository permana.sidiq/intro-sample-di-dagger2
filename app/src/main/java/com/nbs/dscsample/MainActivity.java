package com.nbs.dscsample;

import android.os.Bundle;
import android.widget.Toast;

import com.nbs.dscsample.model.Coffee;
import com.nbs.dscsample.prefs.AppPreference;

import javax.inject.Inject;

public class MainActivity extends BaseActivity {

    @Inject
    Coffee coffee;

    @Inject
    AppPreference appPreference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getAppComponent().inject(this);

        coffee.getCoffeeBean().setName("Arabica");
        coffee.getMilk().setName("Fresh Milk");
        coffee.getSugar().setName("White Sugar");

        Toast.makeText(this, coffee.getCoffeeBean().getName(),
                Toast.LENGTH_SHORT).show();

        appPreference.setName("Sidiq Permana");
        Toast.makeText(this, appPreference.getName(),
                Toast.LENGTH_SHORT).show();
    }
}
