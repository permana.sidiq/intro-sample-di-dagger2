package com.nbs.dscsample.di;

import com.nbs.dscsample.HomeActivity;
import com.nbs.dscsample.MainActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {CoffeeModule.class, PreferenceModule.class})
public interface AppComponent {
    void inject(MainActivity mainActivity);

    void inject(HomeActivity homeActivity);
}
