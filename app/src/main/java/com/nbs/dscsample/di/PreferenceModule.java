package com.nbs.dscsample.di;

import android.content.Context;
import android.content.SharedPreferences;

import com.nbs.dscsample.prefs.AppPreference;

import dagger.Module;
import dagger.Provides;

@Module
public class PreferenceModule {
    private Context context;

    public PreferenceModule(Context context) {
        this.context = context;
    }

    @Provides
    public String provideName(){
        return "DscPref";
    }

    @Provides
    public SharedPreferences provideSharedPreference(String name){
        return context.getSharedPreferences(name, Context.MODE_PRIVATE);
    }

    @Provides
    public AppPreference providePreference(SharedPreferences sharedPreferences){
        return new AppPreference(sharedPreferences);
    }
}
