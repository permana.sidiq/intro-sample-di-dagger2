package com.nbs.dscsample.di;

import com.nbs.dscsample.model.Coffee;
import com.nbs.dscsample.model.CoffeeBean;
import com.nbs.dscsample.model.Milk;
import com.nbs.dscsample.model.Sugar;

import dagger.Module;
import dagger.Provides;

@Module
public class CoffeeModule {
    @Provides
    public CoffeeBean provideCoffeeBean(){
        return new CoffeeBean();
    }

    @Provides
    public Sugar provideSugar(){
        return new Sugar();
    }

    @Provides
    public Milk provideMilk(){
        return new Milk();
    }

    @Provides
    public Coffee provideCoffee(Sugar sugar, CoffeeBean coffeeBean, Milk milk){
        return new Coffee(sugar, coffeeBean, milk);
    }
}
