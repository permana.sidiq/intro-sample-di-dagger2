package com.nbs.dscsample;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.nbs.dscsample.di.AppComponent;
import com.nbs.dscsample.di.CoffeeModule;
import com.nbs.dscsample.di.DaggerAppComponent;
import com.nbs.dscsample.di.PreferenceModule;

public abstract class BaseActivity extends AppCompatActivity {

    private AppComponent appComponent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        appComponent = DaggerAppComponent.builder()
                .coffeeModule(new CoffeeModule())
                .preferenceModule(new PreferenceModule(this))
                .build();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }
}
