package com.nbs.dscsample.prefs;

import android.content.SharedPreferences;

public class AppPreference {
    public static final String KEY_NAME = "key_name";

    private SharedPreferences sharedPreferences;

    public AppPreference(SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
    }

    public void setName(String name){
        sharedPreferences.edit().putString(KEY_NAME, name).apply();
    }

    public String getName(){
        return sharedPreferences.getString(KEY_NAME, null);
    }
}
