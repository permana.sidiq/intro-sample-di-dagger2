package com.nbs.dscsample.model;

public class CoffeeBean {
    private String name;

    public CoffeeBean() {
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
