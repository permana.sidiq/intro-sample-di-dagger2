package com.nbs.dscsample.model;

public class Coffee {

    private Sugar sugar;

    private CoffeeBean coffeeBean;

    private Milk milk;

    public Coffee(Sugar sugar, CoffeeBean coffeeBean, Milk milk) {
        this.sugar = sugar;
        this.coffeeBean = coffeeBean;
        this.milk = milk;
    }

    public Sugar getSugar() {
        return sugar;
    }

    public CoffeeBean getCoffeeBean() {
        return coffeeBean;
    }

    public Milk getMilk() {
        return milk;
    }
}
