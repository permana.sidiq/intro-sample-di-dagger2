package com.nbs.dscsample.model;

public class Milk {
    private String name;

    public Milk() {
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
