package com.nbs.dscsample.model;

public class Sugar {
    private String name;

    public Sugar() {
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
