package com.nbs.dscsample;

import android.os.Bundle;

import com.nbs.dscsample.model.Coffee;
import com.nbs.dscsample.prefs.AppPreference;

import javax.inject.Inject;

public class HomeActivity extends BaseActivity {

    @Inject
    Coffee coffee;

    @Inject
    AppPreference appPreference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        getAppComponent().inject(this);

        appPreference.setName("Deni Rohimat");
    }
}
